<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Index page</title>
<link rel="stylesheet" type="text/css" href="./resources/css/style.css"/>
</head>
<body id="body"> 
    <h1>Welcome to Airport Management System</h1>
    <p align="center"><a href="register">AdminRegistration</a> &nbsp; <a href="adminlogin">AdminLogin</a>
    <a href="managerRegister">ManagerRegistration</a> &nbsp; <a href="managerlogin">ManagerLogin</a> </p>
</body>
</html>
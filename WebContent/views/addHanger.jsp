<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add Hanger</title>

<script type="text/javascript" src="./resources/js/validation.js"></script>
</head>
<body>
<div id="addplane">
<h1>Add Hangars</h1>
<f:form name="fm" onsubmit="return hangarValid()" action="inserthangar" method="post" modelAttribute="h">
<table>
<tr><td>PlaneName</td><td><f:input path="planeName"/></td><td></td><td>HangarName</td><td><f:input path="hangarName"/></td></tr>
<tr><td>Source</td><td><f:input path="source"/></td><td></td><td>HangarCapacity</td><td><f:input path="hangarCapacity"/></td></tr>
<tr><td>Destination</td><td><f:input path="destination"/></td><td></td><td>HangarLocation</td><td><f:input path="hangarLocation"/></td></tr>
<tr><td>NumberOfSeats</td><td><f:input path="numberOfSeats"/></td><td></td><td>Status</td><td><f:input path="status"/></td></tr>
<tr><td></td></tr>
<tr><td></td></tr>
<tr><td></td></tr>
<tr><td></td><td></td><td><input type="submit" value="Add"></td></tr>
</table>
</f:form>
</div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add new Plane Details</title>
</head>
<body>

<div id="addplane">
<h1>Add Planes/Pilots Page</h1>
<f:form name="fm" onsubmit="return planeValid()" action="inserplane" method="post" modelAttribute="a">
<table>
<tr><td>PlaneName</td><td><f:input path="planeName"/></td><td></td><td>PilotId</td><td><f:input path="pilotId"/></td></tr>
<tr><td>Source</td><td><f:input path="source"/></td><td></td><td>PilotName</td><td><f:input path="pilotName"/></td></tr>
<tr><td>Destination</td><td><f:input path="destination"/></td><td></td><td>PilotName</td><td><f:input path="pilotName"/></td></tr>
<tr><td>NumberOfSeats</td><td><f:input path="numberOfSeats"/></td><td></td><td>Age</td><td><f:input path="age"/></td></tr>
<tr><td></td><td></td><td></td><td>Gender</td><td><f:radiobutton path="gender" value="male"/>Male &nbsp;<f:radiobutton path="gender" value="female"/>Female</td></tr>
<tr><td></td><td></td><td></td><td>ContactNumber</td><td><f:input path="contactNumber"/></td></tr>
<tr></tr>
<tr></tr>
<tr></tr>
<tr></tr>
<tr></tr>
<tr><td></td><td></td><td><input type="submit" value="Add"></td></tr>
</table>
</f:form>
</div>

</body>
</html>
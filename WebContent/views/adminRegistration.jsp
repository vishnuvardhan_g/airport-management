<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin registration</title>
<script type="text/javascript" src="./resources/js/validation.js"></script>
</head>
<body>
<div>
<h1>Admin Registration </h1>
<br/>
<p align="right"><a href="index.jsp">Home</a></p>
<f:form name="fm" onsubmit="return adminRegisterValid()" action="addadmin" method="post" modelAttribute="a">
<table>
<tr><td>FirstName</td><td><f:input path="firstName"/></td></tr>
<tr><td>LastName</td><td><f:input path="lastName"/></td></tr>
<tr><td>Age</td><td><f:input path="age"/></td></tr>
<tr><td>Gender</td><td><f:radiobutton path="gender" value="male"/>Male &nbsp;<f:radiobutton path="gender" value="female"/>Female</td></tr>
<tr><td>ContactNumber</td><td><f:input path="contactNumber"/></td></tr>
<tr><td>VendorId</td><td><f:input path="vendorId"/></td></tr>
<tr><td>Password</td><td><f:password path="password"/></td></tr>
<tr><td></td><td><input type="submit" value="Register"></td></tr>
</table>
</f:form>
</div>
</body>
</html>
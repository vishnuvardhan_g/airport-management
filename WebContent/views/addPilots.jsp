<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add new Pilot</title>
</head>
<body>
<h1>Add Pilots Page</h1>
<f:form action="insertpilot" method="post" modelAttribute="i">
<table>
<tr><td>PilotId</td><td><f:input path="pilotId"/></td></tr>
<tr><td>PilotName</td><td><f:input path="pilotName"/></td></tr>
<tr><td>Age</td><td><f:input path="age"/></td></tr>
<tr><td>Gender</td><td><f:radiobutton path="gender"/>Male &nbsp;<f:radiobutton path="gender"/>Female</td></tr>
<tr><td>ContactNumber</td><td><f:input path="contactNumber"/></td></tr>
<tr><td></td><td><f:button>Add Pilot</f:button></td></tr>
</table>
</f:form>
</body>
</html>
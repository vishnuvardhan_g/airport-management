<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Manager Login Page</title>
<script type="text/javascript" src="./resources/js/validation.js"></script>
</head>
<body>
<div>
<span class="fa fa-user-circle fa-5x" ></span>
<h1>Manager Login </h1>
<h4 style="color:red">${mv}</h4>
<f:form name="fm" onsubmit="return managerValid()"   action="managervalid" method="post" modelAttribute="m">
<table>
<tr><td>ManagerId</td><td><f:input path="manager_Id" autocomplete="off"/></td></tr>
<tr><td>Password</td><td><f:password path="password"/></td></tr>
<tr><td></td><td><input type="submit" value="Login"></td></tr>
</table>
</f:form>
</div>

</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin Login Page</title>
</head>
<body>
<div>
<span class="fa fa-user-circle fa-5x" ></span>
<h1>Admin Login </h1>
<h4 style="color:red">${av}</h4>
<f:form name="fm" onsubmit="return adminValid()" action="adminvalid" method="post" modelAttribute="a">
<table>
<tr><td>VendorId</td><td><f:input path="vendor_Id" autocomplete="off"/></td></tr>
<tr><td>Password</td><td><f:password path="password"/></td></tr>
<tr><td></td><td><input type="submit" value="Login"></td></tr>
</table>
</f:form>
</div>

</body>
</html>